package com.gitee.occo.exchange.common.enums;

import java.util.Arrays;
import java.util.Optional;

import lombok.Getter;

@Getter
public enum TradePair {
	
	BTC_USDT(0,"BTC","USDT");
	
	private int code;
	
	private String one;
	
	private String two;
	
	private TradePair(int code, String one, String two) {
		this.code = code;
		this.one = one;
		this.two = two;
	}
	
	public static Optional<TradePair> of(int code) {
		return Arrays.stream(values()).filter(i -> i.code == code).findFirst();
	}
}
