package com.gitee.occo.exchange.common.enums;

import java.util.Arrays;
import java.util.Optional;
import lombok.Getter;

/**
 * 订单动作
 * @author kinbug 
 */
@Getter
public enum OrderAction {
    BID(0),
    ASK(1);

    private byte code;

    OrderAction(int code) {
        this.code = (byte) code;
    }

    public static Optional<OrderAction> of(int code) {
        return Arrays.stream(values()).filter(i -> i.code == code).findFirst();
    }

    public OrderAction opposite() {
        return this == BID ? ASK : BID;
    }

}
