package com.gitee.occo.exchange.fund;

import org.springframework.stereotype.Component;

import com.gitee.occo.exchange.entity.FundOperatesDto;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class FundOperates{

	public Boolean frozen(FundOperatesDto fundOperatesDto) {
		log.info("====远程调用：资金冻结成功！");
		return true;
	}

	public Boolean transfer(FundOperatesDto fundOperatesDto) {
		log.info("====远程调用：资金划转成功！");
		return true;
	}
	
	public Boolean unfreeze(FundOperatesDto fundOperatesDto) {
		log.info("====远程调用：资金解冻成功！");
		return true;
	}
}
