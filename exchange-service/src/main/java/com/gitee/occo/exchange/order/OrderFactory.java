package com.gitee.occo.exchange.order;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.util.Assert;

import com.gitee.occo.exchange.common.enums.OrderType;
import com.gitee.occo.exchange.order.service.NewOrderService;

/**
 * @ClassName: OrderFactory
 * @Description: TODO(下单策略工厂)
 * @author exchange-service
 * @date 2020年6月1日
 */
public class OrderFactory {
	private static Map<Integer,NewOrderService> services = new ConcurrentHashMap<Integer,NewOrderService>();

    public  static NewOrderService getByOrderType(Integer orderType){
        return services.get(orderType);
    }

    public static void register(OrderType orderType,NewOrderService newOrderService){
        Assert.notNull(orderType,"orderType can't be null");
        services.put(orderType.getCode(),newOrderService);
    }
}
