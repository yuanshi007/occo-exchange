package com.gitee.occo.exchange.order;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gitee.occo.exchange.common.entity.Order;

@RestController
@RequestMapping("/exchange")
public class OrderController {

	@GetMapping("/neworder")
	public Order NewOrder(Order order) {
		return OrderFactory.getByOrderType(order.getOrderType()).NewOrder(order);
	}
}
